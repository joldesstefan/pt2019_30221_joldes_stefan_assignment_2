package structuradatecoada;

import java.util.Iterator;
import java.util.TreeSet;

public class Coada {

	public TreeSet<Client>coada;
	public Coada()
	{
		coada=new TreeSet<Client>();
	}
	public void insereazaClient(Client client)
	{
		coada.add(client);
	}
	public boolean isMore()
	{

		Iterator<Client> i=coada.iterator();
		if(i.hasNext())
		{return true;
		}
		else
		{
			return false;
		}
	}
	public  synchronized Client extrageClient()
	{
		Client a,b;
		Iterator<Client> i=coada.iterator();
		if(i.hasNext())
		{
			a=i.next();
			 b=new Client(a.Id,0,0);

			b.timeArrive=a.timeArrive;
			b.timeLeft=a.timeLeft;
			b.timeNeeded=a.timeNeeded;
			b.timeWait=a.timeWait;
			i.remove();
			return b;
		}
		else
		{return null;
		}
	}
	public synchronized String toString()
	{
		String s="";
		Iterator <Client>i=this.coada.iterator();
		while(i.hasNext())
		{Client a=i.next();
			s=s+a.Id+" | ";
			
		}
		return s;
	}
}
