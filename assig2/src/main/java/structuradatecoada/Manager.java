package structuradatecoada;

import java.util.ArrayList;
import java.util.Random;

import gui.ModelView;
import gui.ViewView;

public class Manager implements Runnable {
public int nrCase;
public int time, simTime;
public ArrayList<Casa>caseMarcat=new ArrayList<Casa>();
public int minTime, maxTime,maxSTime,minSTime;
ViewView view;
ArrayList<ModelView> model ;

public  Manager(int nrCase, int minTime, int maxTime, int simTime, int minSTime, int maxSTime,ArrayList<ModelView> model ,ViewView view)
{
	this.model=model;
	this.nrCase=nrCase;
this.minTime=minTime;
this.maxTime=maxTime;
this.simTime=simTime;
this.maxSTime=maxSTime;
this.minSTime=minSTime;
this.view=view;
this.time=0;
}
	public  void run() {
		
		Random random=new Random();
		for(int i=0;i<this.nrCase;i++)
		{
			Casa a=new Casa(simTime,model.get(i),view);
		
		Thread t=new Thread (a);
		
		caseMarcat.add(a);
		t.start();
		
	} 
		int rT,rN;

		while (time<simTime)
		{
			rT=random.nextInt(maxTime-minTime)+minTime;
			
			rN=random.nextInt((maxSTime-minTime))+minSTime;
			int min=caseMarcat.get(0).timeToWait;
		try {
			Thread.sleep(rT);
			time=time+rT;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int pozmin=0;
		
		for (int i=1;i<nrCase;i++)
		{if(min>caseMarcat.get(i).timeToWait)
		{
			min=caseMarcat.get(i).timeToWait;
			pozmin=i;
		}
		
		}
		caseMarcat.get(pozmin).insereazaClient(rN);
	}
	
	

}
}
