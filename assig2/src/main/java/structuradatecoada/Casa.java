package structuradatecoada;

import java.util.*;

import gui.ControllerView;
import gui.ModelView;
import gui.ViewView;
import staticpkc.StaticID;

public class Casa implements Runnable {
	public int IDc;
	public int time;
	public Coada coada;
	public int timeToWait;
	public int timeWaited;
	public int numberServed;
	public int peekNumber;
	public int emptyTime;
	public int simTime;
	public ModelView model;
	public ViewView view;

	public int peekTime;

	public Casa(int simTime, ModelView model, ViewView view) {
		this.view = view;
		this.model = model;
		StaticID.IDC = StaticID.IDC + 1;
		this.IDc = StaticID.IDC;
		emptyTime = 0;
		peekNumber = 0;
		peekTime = 0;
		time = 0;
		this.simTime = simTime;
		timeToWait = 0;
		timeWaited = 0;
		numberServed = 0;
		coada = new Coada();
	}

	public synchronized void insereazaClient(int timeNedded) {
		if (peekNumber < this.coada.coada.size()+1) {
			peekNumber = this.coada.coada.size()+1;
			peekTime = time;

		}
		timeToWait = timeToWait + timeNedded;
		Client client = new Client(time, timeNedded);
		this.coada.insereazaClient(client);
		System.out.println("Timp : " + time + " Client gets in clientId= " + client.Id + " casa " + this.IDc + "\n");
		System.out.println(this.coada.toString() + "\n");
		model.coadaCasa = this.coada.toString()+"To Wait="+this.timeToWait;
		view.actualizaezaCoada(this.IDc);
		view.writeLog("Timp : " + time + " Client gets in clientId= " + client.Id + " casa " + this.IDc + "\n");
	}

	public synchronized Client extrageClient() {
		Client a = this.coada.extrageClient();
		System.out.println(coada.toString() + "\n");
		timeToWait = timeToWait - a.timeNeeded;// a;//
												// this.coada.extrageClient();

		model.coadaCasa = this.coada.toString()+"To Wait="+this.timeToWait;
		view.actualizaezaCoada(this.IDc);

		return a;
	}

	public void run() {
		time = 0;
		// TODO Auto-generated method stub
int ok=0,br=0;
		while (time < simTime) {
			ok=0;
			br=0;
			if (coada.isMore()) {
				Client a = this.extrageClient();
				a.timeLeft = time;
				 ok = 1;
				int timp = a.timeNeeded;
				System.out.println("Timp : " + time + " Client gets out " + a.Id + "casa " + this.IDc + " had to wait "
						+ (a.timeLeft - a.timeArrive) + "\n");
				view.writeLog("Timp : " + time + " Client gets out clientId= " + a.Id + " casa " + this.IDc + " had to wait "
						+ (a.timeLeft - a.timeArrive) + "\n");

				timeWaited = timeWaited + a.timeLeft - a.timeArrive;
				numberServed = numberServed + 1;
				while (timp > 0) {
					if (time >= simTime) {
						System.out.println("Timp : " + time + " Client PROCESS INTERUPTED " + a.Id);
						view.writeLog("Timp : " + time + " Client PROCESS INTERUPTED *casa* "+this.IDc+" client " + a.Id+"\n");
						br = 1;
						break;

					}

					timp = timp - 1;
					time = time + 1;
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			
			}
			if(br==1)
			{
			 break;
			}
		
			if (ok == 1) {
				// System.out.println("Timp : " + time+" Client gets out "+
				// a.Id+" had to wait "+ (a.timeLeft-a.timeArrive)+"\n");

			} else {
				try {
					Thread.sleep(10);
					time = time + 10;
this.emptyTime=this.emptyTime+10;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Simulation OVER  *casa* "+this.IDc+"at time inside : " + this.time+" Medium time waited "+(this.timeWaited/(this.numberServed+1))+" Peek number at time peek "+this.peekNumber+" "+this.peekTime+" Total empty time "+this.emptyTime);
view.writeLog("Simulation OVER  *casa* "+this.IDc+"at time inside : " + this.time+" Medium time waited "+(this.timeWaited/(this.numberServed+1))+" Peek number at time peek "+this.peekNumber+" "+this.peekTime+" Total empty time "+this.emptyTime+"\n");

	}

	public static void main(String args[]) throws InterruptedException

	{
		/*
		 * //c.extrageClient();
		 * 
		 * //System.out.println(c.toString()); Casa a=new Casa(); Thread t=new
		 * Thread(a); t.start(); Casa b=new Casa(); Thread tt=new Thread(b);
		 * tt.start(); Random random=new Random(); for(int i=1;i<=5;i++) { int
		 * rT,rN; rT=random.nextInt(50); rN=random.nextInt(500);
		 * 
		 * if(a.timeToWait<b.timeToWait) { a.insereazaClient(rN); } else {
		 * b.insereazaClient(rN);
		 * 
		 * } Thread.sleep(rT);
		 * 
		 * }
		 */
		// ModelView model=new ModelView();
		ArrayList<ModelView> modell = new ArrayList<ModelView>();
		for (int i = 1; i <= 5; i++) {
			modell.add(new ModelView());
		}
		ViewView vieww = new ViewView(modell);
		ControllerView c=new ControllerView(vieww);
		
	}
}
