package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ViewView {
	public ArrayList<ModelView> model;

	public JFrame frame = new JFrame("Simulator - Queques");
	public JPanel pane = new JPanel();
	public JButton butonStart = new JButton("Incepe");
	public JTextArea textPolinomSursaA = new JTextArea();
	public JTextArea textPolinomSursaB = new JTextArea();
	public JTextArea textPolinomSursaC = new JTextArea();
	public JTextArea textPolinomSursaD = new JTextArea();
	public JTextArea textPolinomSursaE = new JTextArea();
	public JTextArea textPolinomSursaF = new JTextArea();

	public JScrollPane scrollA = new JScrollPane(textPolinomSursaA);
	public JScrollPane scrollB = new JScrollPane(textPolinomSursaB);
	public JScrollPane scrollC= new JScrollPane(textPolinomSursaC);
	public JScrollPane scrollD = new JScrollPane(textPolinomSursaD);
	public JScrollPane scrollE = new JScrollPane(textPolinomSursaE);
	public JScrollPane scrollF = new JScrollPane(textPolinomSursaF);

	public JTextArea textNrCase = new JTextArea();
	public JTextArea textMinTime = new JTextArea();
	public JTextArea 	textMaxTime = new JTextArea();
	public JTextArea textMinService = new JTextArea();
	public JTextArea textMaxService = new JTextArea();
	public JTextArea textSimTime = new JTextArea();
	public JTextArea textNrCaset = new JTextArea();
	public JTextArea textMinTimet = new JTextArea();
	public JTextArea 	textMaxTimet = new JTextArea();
	public JTextArea textMinServicet = new JTextArea();
	public JTextArea textMaxServicet = new JTextArea();
	public JTextArea textSimTimet = new JTextArea();


	


	



	

	


	

	



	public ViewView(ArrayList<ModelView> model) {
		this.model = model;
		frame.setBounds(20, 200, 1880, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// pane.setBounds(10, 10, 100, 100);
		pane.setBackground(Color.white);
		pane.setLayout(null);
		butonStart.setBounds(1600, 10, 200, 50);


		butonStart.setBackground(new Color(62, 67, 222));
		butonStart.setForeground(Color.WHITE);


		pane.add(butonStart);
		
		scrollA.setBounds(50, 50, 1500, 50);
		textPolinomSursaA.setBackground(new Color(163, 163, 163));
		scrollA.setBorder(null);
		textPolinomSursaA.setFont(textPolinomSursaA.getFont().deriveFont(25f));
		textPolinomSursaA.setEditable(false);

		scrollB.setBounds(50, 150, 1500, 50);
		textPolinomSursaB.setBackground(new Color(163, 163, 163));
		scrollB.setBorder(null);
		textPolinomSursaB.setFont(textPolinomSursaB.getFont().deriveFont(25f));
		textPolinomSursaB.setEditable(false);

		scrollC.setBounds(50, 250, 1500, 50);
		textPolinomSursaC.setBackground(new Color(163, 163, 163));
		scrollC.setBorder(null);
		textPolinomSursaC.setFont(textPolinomSursaC.getFont().deriveFont(25f));
		textPolinomSursaC.setEditable(false);

		scrollD.setBounds(50, 350, 1500, 50);
		textPolinomSursaD.setBackground(new Color(163, 163, 163));
		scrollD.setBorder(null);
		textPolinomSursaD.setFont(textPolinomSursaD.getFont().deriveFont(25f));
		textPolinomSursaD.setEditable(false);

		scrollE.setBounds(50, 450, 1500, 50);
		textPolinomSursaE.setBackground(new Color(163, 163, 163));
		scrollE.setBorder(null);
		textPolinomSursaE.setFont(textPolinomSursaE.getFont().deriveFont(25f));
		textPolinomSursaE.setEditable(false);

		
		scrollF.setBounds(50, 550, 1500, 400);
		textPolinomSursaF.setBackground(new Color(163, 163, 163));
		scrollF.setBorder(null);
		textPolinomSursaF.setFont(textPolinomSursaF.getFont().deriveFont(25f));
		textPolinomSursaF.setEditable(false);
		
		
		
		textNrCase.setBounds(1600, 50, 100, 50);
		//textNrCase.setEditable(true);
		textNrCase.setBackground(new Color(163, 163, 163));
		textNrCase.setBorder(null);
		textNrCase.setFont(textNrCase.getFont().deriveFont(25f));
		//textNrCase.setEditable(false);
		
	
		
		textSimTime.setBounds(1600, 150, 100, 50);
		//textSimTime.setEditable(false);
		textSimTime.setBackground(new Color(163, 163, 163));
		textSimTime.setBorder(null);
		textSimTime.setFont(textNrCase.getFont().deriveFont(25f));
		//textSimTime.setEditable(false);
		
		textMinTime.setBounds(1600, 250, 100, 50);
		//textMinTime.setEditable(false);
		textMinTime.setBackground(new Color(163, 163, 163));
		textMinTime.setBorder(null);
		textMinTime.setFont(textNrCase.getFont().deriveFont(25f));
		//textMinTime.setEditable(false);
		
		textMaxTime.setBounds(1600, 350, 100, 50);
		//textMaxTime.setEditable(false);
		textMaxTime.setBackground(new Color(163, 163, 163));
		textMaxTime.setBorder(null);
		textMaxTime.setFont(textNrCase.getFont().deriveFont(25f));
		//textMaxTime.setEditable(false);
		
		textMinService.setBounds(1600,450, 100, 50);
		//textMinService.setEditable(false);
		textMinService.setBackground(new Color(163, 163, 163));
		textMinService.setBorder(null);
		textMinService.setFont(textNrCase.getFont().deriveFont(25f));
		//textMinService.setEditable(false);
		
		
		textMaxService.setBounds(1600,550, 100, 50);
		//textMaxService.setEditable(false);
		textMaxService.setBackground(new Color(163, 163, 163));
		textMaxService.setBorder(null);
		textMaxService.setFont(textNrCase.getFont().deriveFont(25f));
		//textMaxService.setEditable(false);
		
		textNrCaset.setBounds(1700, 50, 100, 50);
		//textNrCase.setEditable(true);
		textNrCaset.setBackground(new Color(163, 163, 163));
		textNrCaset.setBorder(null);
		textNrCaset.setFont(textNrCaset.getFont().deriveFont(25f));
		//textNrCase.setEditable(false);
		textNrCaset.setText("nrCase");
	
		
		textSimTimet.setBounds(1700, 150, 100, 50);
		//textSimTime.setEditable(false);
		textSimTimet.setBackground(new Color(163, 163, 163));
		textSimTimet.setBorder(null);
		textSimTimet.setFont(textSimTimet.getFont().deriveFont(25f));
		//textSimTime.setEditable(false);
		textSimTimet.setText("timeS");

		textMinTimet.setBounds(1700, 250, 100, 50);
		//textMinTime.setEditable(false);
		textMinTimet.setBackground(new Color(163, 163, 163));
		textMinTimet.setBorder(null);
		textMinTimet.setFont(textMinTimet.getFont().deriveFont(25f));
		//textMinTime.setEditable(false);
		textMinTimet.setText("minArr");

		textMaxTimet.setBounds(1700, 350, 100, 50);
		//textMaxTime.setEditable(false);
		textMaxTimet.setBackground(new Color(163, 163, 163));
		textMaxTimet.setBorder(null);
		textMaxTimet.setFont(textMaxTimet.getFont().deriveFont(25f));
		//textMaxTime.setEditable(false);
		textMaxTimet.setText("maxArr");

		textMinServicet.setBounds(1700,450, 100, 50);
		//textMinService.setEditable(false);
		textMinServicet.setBackground(new Color(163, 163, 163));
		textMinServicet.setBorder(null);
		textMinServicet.setFont(textMinServicet.getFont().deriveFont(25f));
		//textMinService.setEditable(false);
		textMinServicet.setText("minSer");

		
		textMaxServicet.setBounds(1700,550, 100, 50);
		//textMaxService.setEditable(false);
		textMaxServicet.setBackground(new Color(163, 163, 163));
		textMaxServicet.setBorder(null);
		textMaxServicet.setFont(textMaxServicet.getFont().deriveFont(25f));
		//textMaxService.setEditable(false);
		textMaxServicet.setText("maxSer");

	
		pane.add(scrollA);
		pane.add(scrollB);
		pane.add(scrollC);
		pane.add(textMinTime);
		pane.add(textMaxTime);
		pane.add(textNrCase);
		pane.add(textSimTime);
		pane.add(textMinService);
		pane.add(textMaxService);
		pane.add(textMinTimet);
		pane.add(textMaxTimet);
		pane.add(textNrCaset);
		pane.add(textSimTimet);
		pane.add(textMinServicet);
		pane.add(textMaxServicet);
		pane.add(scrollD);
		pane.add(scrollE);
		pane.add(scrollF);

		frame.setContentPane(pane);

	
		frame.setVisible(true);
	}

	public void actualizaezaCoada(int poz) {
		this.textPolinomSursaA.setText(this.model.get(0).coadaCasa);
		this.textPolinomSursaB.setText(this.model.get(1).coadaCasa);

		this.textPolinomSursaC.setText(this.model.get(2).coadaCasa);

		this.textPolinomSursaD.setText(this.model.get(3).coadaCasa);

		this.textPolinomSursaE.setText(this.model.get(4).coadaCasa);

	}
	public synchronized  void writeLog(String sir)
	{
		textPolinomSursaF.append(sir);
	}
	public String getNrCase()
	{
		return this.textNrCase.getText();
	}
	public String getMinTime()
	{
		return this.textMinTime.getText();
	}
	public String getMaxTime()
	{
		return this.textMaxTime.getText();
	}
	public String getMinService()
	{
		return this.textMinService.getText();
	}
	public String getMaxService()
	{
		return this.textMaxService.getText();
	}
	public String getSimTime()
	{
		return this.textSimTime.getText();
	}

	public void addActionStart(ActionListener clasaActiune) {
butonStart.addActionListener(clasaActiune);	}




}
